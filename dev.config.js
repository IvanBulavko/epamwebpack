const path = require("path");
const fs = require("fs");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const PrettierPlugin = require("prettier-webpack-plugin");

const PATHS = {
  pages: path.join(__dirname, "./src/pages"),
};

const PAGES_DIR = `${PATHS.pages}/`;
const PAGES = fs
  .readdirSync(PAGES_DIR)
  .filter((fileName) => fileName.endsWith(".pug"));

module.exports = {
  entry: "./src/pages/file.js",
  mode: "development",
  module: {
    rules: [
      {
        test: /\.pug$/,
        exclude: /(node_modules)|package.json/,
        use: ["pug-loader"],
      },
      {
        test: /\.js$/,
        exclude: /(node_modules)|package.json/,
        use: {
          loader: "eslint-loader",
        },
      },
      {
        test: /\.scss$/,
        exclude: /(node_modules)|package.json/,

        use: ["style-loader", "css-loader", "sass-loader"],
      },
    ],
  },
  plugins: [
    ...PAGES.map(
      (page) =>
        new HtmlWebpackPlugin({
          template: `${PAGES_DIR}/${page}`,
          filename: `./${page.replace(/\.pug/, ".html")}`,
        })
    ),
    new PrettierPlugin({
      tabWidth: 2,
    }),
  ],
  output: {
    clean: true, // Clean the output directory
  },
  optimization: {
    splitChunks: {
      chunks: "all",
    },
  },
  stats: {
    errorDetails: true,
  },
};
